
import WebtelkomService from '@/services/api-services/WebtelkomService.js'

export const state = {
    Services: [
        {
          id: 1,
          image:
            "https://sdk-mobile-telkom-air.s3-ap-southeast-1.amazonaws.com/image_aetra_xxx.png",
          brandTitle: "Aetra",
          companyName: "PT Aetra Air Jakarta",
          desc:
            "Tagihan Perusahaan Listrik Negara berlaku untuk seluruh wilayah Indonesia"
        },
        {
          id: 2,
          image:
            "https://sdk-mobile-telkom-air.s3-ap-southeast-1.amazonaws.com/image_palyja_xxx.png",
          brandTitle: "Palyja",
          companyName: "PT PAM Lyonnaise Jaya",
          desc:
            "Tagihan Perusahaan Listrik Negara berlaku untuk seluruh wilayah Indonesia"
        },
        {
          id: 3,
          image:
            "https://sdk-mobile-telkom-air.s3-ap-southeast-1.amazonaws.com/image_pam_xxx.png",
          brandTitle: "PAM Jaya",
          companyName: "PT PAM Lyonnaise Jaya",
          desc:
            "Tagihan Perusahaan Listrik Negara berlaku untuk seluruh wilayah Indonesia"
        },
        {
          id: 4,
          image:
            "https://sdk-mobile-telkom-air.s3-ap-southeast-1.amazonaws.com/image_pln_xxx.png",
          brandTitle: "PLN",
          companyName: "PLN Prabayar",
          desc:
            "Tagihan Perusahaan Listrik Negara berlaku untuk seluruh wilayah Indonesia"
        },
        {
          id: 5,
          image:
            "https://sdk-mobile-telkom-air.s3-ap-southeast-1.amazonaws.com/image_telko_xxx.png",
          brandTitle: "Telkom",
          companyName: "PT Telekomunikasi Indonesia",
          desc:
            "Tagihan Perusahaan Listrik Negara berlaku untuk seluruh wilayah Indonesia"
        }
      ],
      inquiry: {},
}


export const mutations = {
  SET_INQUIRY(state, payload){
    state.inquiry = payload
  }
}

export const actions = {
  fetchInquiry({ commit }, payload){
    return WebtelkomService.postServices(payload)
      .then((res) => {
        commit('SET_INQUIRY', res.data)
      })
      .catch(err => {
        throw err
      })
  }
}