import Vue from "vue";
import Vuex from "vuex";
import * as service from '@/store/modules/webtelkom.js'

Vue.use(Vuex);

export default new Vuex.Store({
  modules: {
    service
  },
  state: {}
});
