import Vue from "vue";
import Router from "vue-router";
import List from "./views/PedeTv/List.vue";
import ListDetail from "./views/PedeTv/ListDetail.vue";
import Purchase from "./views/PedeTv/Purchase.vue";

Vue.use(Router);

export default new Router({
  mode: "history",
  base: process.env.BASE_URL,
  routes: [
    {
      path: "/",
      name: "list",
      component: List
    },
    {
      path: "/list/:itemId",
      name: "listDetail",
      component: ListDetail
    },
    {
      path: "/list/:itemId/purchase/:serviceId",
      name: "purchase",
      component: Purchase
    }
  ]
});
