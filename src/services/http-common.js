import axios from 'axios';


export const ApiClient = axios.create({
  baseURL: `https://indoalliz-test.apigee.net`,
  headers: {
    Authorization: TOKEN,
    Accept: 'application/json',
    'Content-Type': 'application/json'
  }
})

// 'Bearer IzMyWFQo2gAEIuGmxXgadNfGC2p7'
// export const HTTP = axios.create({
//   baseURL: `https://indoalliz-test.apigee.net/`,
//   headers: {
//     Authorization: 'Bearer A3GYndqCzoTdio33uwhX2hjHes2s',
//     Accept: 'application/json',
//     'Content-Type': 'application/json'
//   }
// })



