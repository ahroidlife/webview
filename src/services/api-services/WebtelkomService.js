import axios from 'axios';


export const ApiClient = axios.create({
  baseURL: `https://indoalliz-test.apigee.net/indoalliz/api/v1`,
  headers: {
    Authorization: 'Bearer IzMyWFQo2gAEIuGmxXgadNfGC2p7',
    Accept: 'application/json',
    'Content-Type': 'application/json'
  }
})

// 'Bearer IzMyWFQo2gAEIuGmxXgadNfGC2p7'

const endpoint = '/investments/me/transaction-inquiry';
 
export default {
  getServices() {
    return ApiClient.get(endpoint)
  },
  getServices(id) {
    return ApiClient.get(endpoint + id)
  },
  postServices(inquiry) {
    return ApiClient.post(endpoint, inquiry)
  }
}
