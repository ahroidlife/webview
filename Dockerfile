# ---- Base Node ----
FROM node:11.12-alpine AS BASE
WORKDIR /app

# ---- Dependencies ----
FROM BASE AS dependencies  
COPY package*.json ./
COPY package-lock.json ./
RUN npm install

# ---- Copy Files/Build ----
FROM dependencies AS build  
WORKDIR /app
COPY ./ /app
RUN npm run build

# --- Release ----
FROM BASE AS release  

COPY --from=build /app/server ./
COPY --from=build /app/dist ./

RUN npm install pm2 global && \
    npm install express compression connect-history-api-fallback helmet

RUN addgroup -S nodejs && adduser -S -G nodejs nodejs
USER nodejs

CMD ["pm2-docker", "/app/process.yml", "--web", "--json"]
